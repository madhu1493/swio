const express = require('express');
const mongoose = require('mongoose');
const Stripe = require('stripe');
const bodyParser = require('body-parser');
const cors = require('cors');
require('dotenv').config({ path: "./.env" })

const app = express();
const stripe = new Stripe(process.env.stripe_key); // replace with your Stripe secret key
app.use(cors());
app.use(bodyParser.json());

mongoose.connect(process.env.mongoDb, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
}).then(() => console.log('Connected to MongoDB'))
  .catch(err => console.error('Could not connect to MongoDB', err));

const transactionSchema = new mongoose.Schema({
  name: String,
  amount: Number,
  sessionId: String,
});

const Transaction = mongoose.model('Transaction', transactionSchema);

app.get('/', (req, res)=>{
  res.send("Hello world");
})

app.post('/checkout', async (req, res) => {
  const { name, amount } = req.body;
  try {
    const session = await stripe.checkout.sessions.create({
      metadata: {
        amount,
        name,
      },
      line_items: [
        {
          price_data: {
            currency: 'inr',
            product_data: {
              name: 'Sleev subscription',
            },
            unit_amount: amount * 100,
          },
          quantity: 1,
        },
      ],
      mode: 'payment',
      currency: 'inr',
      success_url: `https://swio-frontend.vercel.app/success?session_id={CHECKOUT_SESSION_ID}`,
      cancel_url: `https://swio-frontend.vercel.app/cancel`,
    });

    const transaction = {
      sessionId: session.id,
      name: session.metadata.name,
      amount: session.amount_total / 100, // convert to main currency unit
    };

    // Optionally save the transaction to your database
    const newTransaction = new Transaction(transaction);
    await newTransaction.save();

    res.json({ url: session.url });
  } catch (error) {
    console.error('Error creating checkout session:', error.message);
    res.status(500).json({
      error: 'An error occurred while creating the checkout session.',
    });
  }
});


app.get('/transactions', async (req, res) => {

  if(req.query.sessionId != null && req.query.sessionId != undefined){
    var transationObj = await Transaction.find({sessionId:req.query.sessionId})
    res.send(transationObj);
  }

  var transations = await Transaction.find();
  res.send(transations)
})

app.get('/transactions', async (req, res) => {
  var transationObj = await Transaction.find({sessionId:req.query.sessionId})
  res.send(transationObj);
})



// app.get('/success', async (req, res) => {
//   const sessionId = req.query.session_id;

//   try {
//     const session = await stripe.checkout.sessions.retrieve(sessionId);
//     const transaction = {
//       transactionId: session.payment_intent,
//       name: session.metadata.name,
//       amount: session.amount_total / 100, // convert to main currency unit
//     };

//     // Optionally save the transaction to your database
//     const newTransaction = new Transaction(transaction);
//     await newTransaction.save();

//     res.json(transaction);
//   } catch (error) {
//     console.error('Error retrieving session:', error.message);
//     res.status(500).json({
//       error: 'An error occurred while retrieving the session.',
//     });
//   }
// });




app.listen(3001, () => {
  console.log('Server is running on port 3001');
});
