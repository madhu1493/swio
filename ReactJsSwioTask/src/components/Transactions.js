import React, { useEffect, useState } from 'react';
import './Transactions.css';
import { Link } from 'react-router-dom';

const Transactions = () => {
  const [transactions, setTransactions] = useState([]);
  const baseUrl = process.env.REACT_APP_BaseUrl;

  useEffect(() => {
    fetch(`${baseUrl}/transactions`)
      .then((response) => response.json())
      .then((data) => setTransactions(data))
      .catch((error) => console.error('Error:', error));
  }, []);

  console.log(transactions)

  return (
    <>
      <h1 style={{textAlign:'center'}}>Transactions</h1>
      <div className="transactions-container">
        {transactions.map((transaction) => (
          <div key={transaction.id} className="transaction-card">
            <p className="transaction-name">{transaction.name}</p>
            <p className="transaction-amount">{transaction.amount}</p>
          </div>
        ))}
      </div>
      <Link to='/'>Go To Home</Link>
    </>
  );
}

export default Transactions;
