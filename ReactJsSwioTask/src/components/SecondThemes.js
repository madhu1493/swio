import React from 'react';
import './SecondThemes.css';
import themescorouselimg from '../images/themescorouselimg.png';
import cardImageOne from '../images/card-image-one.png';
import cardImageTwo from '../images/card-image-two.png';


export default function SecondThemes() {
    return (
        <>
            <div className='containersecondthemes'>
                <p className='Secondthemes-para1'>NEW IN  <span style={{ color: 'white', backgroundColor: 'rgb(20, 152, 246)', borderRadius: '50px' }}> &nbsp;&nbsp;2.0 &nbsp;</span></p>
                <p className='Secondthemes-para2'>
                    Themes. Unlimited themes.
                </p>
                <p className='Secondthemes-para3'>Themes in Sleeve make creating and switching between customizations easy. Share your own creations with friends and install as many themes as you like with just a double-click.</p>
                <div className='themescorouselimg'>
                    <img src={themescorouselimg} alt="" />
                </div>
                <div className='Secondthemes-cards-main'>
                    <div className='Secondthemes-card-one'>
                        <div className='Secondthemes-card-one-div-one'>
                            <p style={{fontSize:'25px',fontWeight:'500'}}>Change it up</p>
                            <p style={{fontSize:'20px'}}>Switch between themes with just a click.</p>
                            <p style={{fontSize:'20px'}}>Modify the built-in themes or create your own using Sleeve’s extensive preferences.</p>

                        </div>
                        <div className='Secondthemes-card-one-div-two'>
                            <img src={cardImageOne} alt="" />
                        </div>

                    </div>
                    <div className='Secondthemes-card-two'>
                        <div className='Secondthemes-card-two-div-one'>
                        <p style={{fontSize:'25px',fontWeight:'500'}}>Shareable</p>
                            <p style={{fontSize:'20px'}}>Export your themes and share them with friends using the handy new Sleeve Theme file format.</p>
                            <p style={{fontSize:'20px'}}>Install themes from anywhere with a double-click or a drag and drop.</p>

                        </div>
                        <div className='Secondthemes-card-two-div-two'> 
                            <img src={cardImageTwo} alt="" />
                        </div>

                    </div>
                </div>
            </div>

        </>
    )
}
