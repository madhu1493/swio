import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import { Link } from 'react-router-dom';
const Success = () => {
  const [transaction, setTransaction] = useState(null);
  const location = useLocation();
  const baseUrl = process.env.REACT_APP_BaseUrl;

  useEffect(() => {
    const query = new URLSearchParams(location.search);
    const sessionId = query.get('session_id');

    const fetchSession = async () => {
      try {
        const response = await fetch(`${baseUrl}/transactions?sessionId=${sessionId}`);
        const data = await response.json();
        console.log(data)
        setTransaction(data[0]);
      } catch (error) {
        console.error('Error fetching session:', error);
      }
    };

    if (sessionId) {
      fetchSession();
    }
  }, [location]);

  return (
    <div className="payment-container">
      <h1>Payment Successful</h1>
      {transaction ? (
        <div>
          <p>Transaction ID: {transaction._id}</p>
          <p>Name: {transaction.name}</p>
          <p>Amount: {transaction.amount}</p>
        </div>
      ) : (
        <p>Loading...</p>
      )}
      <Link to='/'>Go To Home</Link>
    </div>
  );
};

export default Success;
