import React from 'react';
import './FirstSection.css';
import applemusicImage from '../images/applemusic.jpg'; 
import spotifyImage from '../images/spotifyimg.png'; 
import dopplerImage from '../images/dopplerimg.jpg'; 
import applelogo from '../images/applelogo.png';
import musictable from '../images/musictable.png';
import { Link } from 'react-router-dom';

export default function FirstSection() {
    return (
        <>
            <div className="containerfirstsection">
                <h1 className="title">
                    Sleeve <span className="title2">2</span>
                </h1>
                <p className="para1">
                    The ultimate music accessory for your Mac.
                </p>
                <p className='para2'>
                    Sleeve sits on the desktop, displaying and controlling the music you’re currently playing in

                    <span>
                        <span> </span>
                        <img className='applemusicimg' src={applemusicImage} alt="Apple Music" />
                        <span className="image-space"></span> 
                    </span>
                    Apple Music,

                    <span>
                        <span> </span>
                        <img className='spotifyimg' src={spotifyImage} alt="Spotify" />
                        <span className="image-space"></span> 
                    </span>
                    Spotify, and
                    <span>
                        <span> </span>
                        <img className='dopplerimg' src={dopplerImage} alt="Doppler" />
                    </span>
                    <span className="image-space"></span>
                    Doppler.
                </p>
                <div className='twoboxdiv'>
                    <div className='applediv'>
                        <img src={applelogo} alt="" />
                        <p>Mac App Store</p>
                    </div>
                    <div className='paymentdiv'>
                    <p>Buy Directly</p>
                    <p style={{color:'white', fontSize:'20px',borderRadius:'10px',backgroundColor:'black'}}> &nbsp;$5.99 &nbsp;</p>
                    </div>
                </div>
                <div className='twoboxdiv'>
                    <div className='applediv'>
                        <Link to="/payment" style={{color:'white'}}><p>Payment</p></Link>
                    </div>
                    <div className='paymentdiv'>
                     <Link to="/viewtransactions"  style={{color:'black'}}><p>View Transactions</p></Link>
                    
                    </div>
                </div>
                <p className='para3'>
                   &nbsp; &nbsp; &nbsp; No subscriptions. No in-app purchases. Requires macOS 11 Big Sur &nbsp; &nbsp; or later.
                </p>
                <div className='progressdiv'> 
                    <img src={musictable} alt="" />
                   <p>Now with <strong> shelves and a progress bar.</strong> See what's new in Sleeve 2.3 </p>
                </div>
            </div>
        </>
    );
}
