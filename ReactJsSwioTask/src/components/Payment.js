

import React, { useState } from 'react';
import './Payment.css';

export default function Payment() {
  const [name, setName] = useState('');
  const [amount, setAmount] = useState('');
  const baseUrl = process.env.REACT_APP_BaseUrl;

  const handleSubmit = async () => {
    try {
      const response = await fetch(`${baseUrl}/checkout`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ name, amount }),
      });
      const data = await response.json();
      if (data.url) {
        window.location.href = data.url; // Redirect to Stripe Checkout
      } else {
        alert('Failed to initiate payment');
      }
    } catch (error) {
      console.error('Error:', error);
      alert('Payment Failed');
    }
  };

  return (
    <div className='payment-container'>
      <h1>Payment Page</h1>
      <p>
        Name: <input style={{ height: '30px' }} type="text" value={name} onChange={(e) => setName(e.target.value)} />
      </p>
      <p>
        Amount: <input style={{ height: '30px' }} type="number" value={amount} onChange={(e) => setAmount(e.target.value)} />
      </p>
      <button onClick={handleSubmit}>Pay</button>
    </div>
  );
}
