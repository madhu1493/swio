import React from 'react';
import './ThirdCustomization.css';
import custImgOne from '../images/cust-img-one.png';
import custImgTwo from '../images/cust-img-two.png';
import custImgThree from '../images/cust-img-three.png';
import custImgFour from '../images/cust-img-four.png';
import custImgFive from '../images/cust-img-five.png';
import custImgSix from '../images/cust-img-six.png';
import custImgSeven from '../images/cust-img-seven.png';
import custImgEight from '../images/cust-img-eight.png';
import custImgNine from '../images/cust-img-nine.png';
import stickyimg from '../images/stickyimg.png';
import artworkImgOne from '../images/artwork-img-one.png'
import artworkImgTwo from '../images/artwork-img-two.png';
import artworkImgThree from '../images/artwork-img-three.png';
import typographyImgOne from '../images/typography-img-one.png';
import typographyImgTwo from '../images/typography-img-two.png';
import interfaceImgOne from '../images/interface-img-one.png';
import interfaceImgTwo from '../images/interface-img-two.png';
import settingsImgOne from '../images/settings-img-one.png';
import settingsImgTwo from '../images/settings-img-two.png';
import settingsImgThree from '../images/settings-img-three.png';

export default function ThirdCustomization() {
    return (
        <>
            <div className='thirdcustom-container'>
                <p className='third-para1'>CUSTOMIZATION</p>
                <p className='third-para2'>Countless ways to customize.</p>
                <p className='third-para3'>Customization is at the core of the Sleeve experience — choose from any combination of design choices, behaviors and settings to make Sleeve at home on your desktop.</p>
                <div className='thirdcustom-images'>
                    <img className='custImgOne' src={custImgOne} alt="" />
                    <img className='custImgTwo' src={custImgTwo} alt="" />
                    <img className='custImgThree' src={custImgThree} alt="" />
                    <img className='custImgFour' src={custImgFour} alt="" />
                    <img className='custImgFive' src={custImgFive} alt="" />
                    <img className='custImgSix' src={custImgSix} alt="" />
                    <img className='custImgSeven' src={custImgSeven} alt="" />
                    <img className='custImgEight' src={custImgEight} alt="" />
                    <img className='custImgNine' src={custImgNine} alt="" />

                </div>

            </div>
            <div className='sticky-container'>
                <img className='stickyimg' src={stickyimg} alt="" />
                <div className='thirdcust-artwork'>
                    <div className='artwork-left'>
                        <img  style={{ height: '100px', width: '100px' }} src={custImgEight} alt="" />
                        <p style={{ fontSize: '48px', fontWeight: '700' }}>Artwork</p>
                        <p style={{ fontSize: '25px' }}>Scale artwork all the way up or all the way down. Round the corners or leave them square.</p>
                        <p style={{ fontSize: '25px' }}>Choose shadow and lighting effects to bring your album artwork to life.</p>
                        <p style={{ fontSize: '25px' }}>Or hide it completely.</p>

                    </div>
                    <div className='artwork-right'>
                        <img className='artworkimgone' src={artworkImgOne} alt="" />
                        <img className='artworkimgtwo' src={artworkImgTwo} alt="" />
                        <img className='artworkimgthree' src={artworkImgThree} alt="" />
                    </div>
                </div>
                <div className='thirdcust-typography'>
                    <div className='typography-left'>
                        <img style={{ height: '100px', width: '100px' }} src={custImgSix} alt="" />
                        <p style={{ fontSize: '48px', fontWeight: '700' }}>Typography</p>
                        <p style={{ fontSize: '25px' }}>Pick the track info you want to display, and then exactly how to display it.</p>
                        <p style={{ fontSize: '25px' }}>Choose the fonts, weights, sizes, and transparency to use for each line, along with customizing color and shadow.</p>

                    </div>
                    <div className='typography-right'>
                        <img className='typographyimgone' src={typographyImgOne} alt="" />
                        <img className='typographyimgtwo' src={typographyImgTwo} alt="" />
                    </div>
                </div>
                <div className='thirdcust-interface'>
                    <div className='interface-left'>
                        <img style={{ height: '100px', width: '100px' }} src={custImgFour} alt="" />
                        <img style={{ height: '100px', width: '100px', position: 'relative', top: '12px', right: '60px' }} src={custImgSeven} alt="" />
                        <img style={{ height: '100px', width: '100px', position: 'relative', top: '20px', right: '100px' }} src={custImgThree} alt="" />

                        <p style={{ fontSize: '48px', fontWeight: '700' }}>Interface</p>
                        <p style={{ fontSize: '25px' }}>Customize the layout, alignment and position to fit your setup.</p>
                        <p style={{ fontSize: '25px' }}>Show and hide playback controls. Add a backdrop layer and customize it.</p>

                    </div>
                    <div className='interface-right'>
                        <img className='interfaceImgOne' src={interfaceImgOne} alt="" />
                        <img className='interfaceImgTwo' src={interfaceImgTwo} alt="" />
                    </div>
                </div>
                <div className='thirdcust-settings'>
                    <div className='settings-left'>
                        <img style={{ height: '100px', width: '100px' }} src={custImgTwo} alt="" />
                        <img style={{ height: '100px', width: '100px', position: 'relative', top: '12px', right: '60px' }} src={custImgOne} alt="" />
                        <img style={{ height: '100px', width: '100px', position: 'relative', top: '20px', right: '100px' }} src={custImgNine} alt="" />

                        <p style={{ fontSize: '48px', fontWeight: '700' }}>Settings</p>
                        <p style={{ fontSize: '25px' }}>
                        Decide if Sleeve stays out of the way, behind windows, or in front of them — or only when you need to see it.
                        </p>
                        <p style={{ fontSize: '25px' }}>Show it in the Dock, choose from custom icons, or keep it on the Desktop only.</p>
                        <p style={{ fontSize: '25px' }}>Set your custom keyboard shortcuts and integrate with the apps you use.</p>

                    </div>
                    <div className='settings-right'>
                        <img className='settingsImgOne' src={settingsImgOne} alt="" />
                        <img className='settingsImgTwo' src={settingsImgTwo} alt="" />
                        <img className='settingsImgThree' src={settingsImgThree} alt="" />
                    </div>
                </div>
            </div>
            <div className='Integrations'>
                <p className='Integrations-para1'>INTEGRATIONS</p>
                <p className='Integrations-para2'> Like, Scrobble.</p>
            </div>
        </>
    )
}
