import './App.css';
import FirstSection from './components/FirstSection';
import SecondThemes from './components/SecondThemes';
import ThirdCustomization from './components/ThirdCustomization';

function App() {
  return (
    <div className="App">
      <FirstSection/>
      <SecondThemes/>
      <ThirdCustomization/>
    </div>
  );
}

export default App;
